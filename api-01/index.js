const app = require('express')();
var jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    extended: false
}));


app.get('/users', (req, res) => {
    console.log('originalUrl', req.originalUrl)
    console.log('param', req.params)
    console.log('path', req.path)
    res.send('Hello 01 API')
});

app.get('/users/:param', (req, res) => {
    res.send(req.params)
});

app.get('/users/:param1/:param2', (req, res) => {
    res.send(req.params)
});


app.post('/login', (req, res) => {
    console.log(req.body)
    if (req.body.user == 'admin' && req.body.pwd == 'admin') {
        const id = 1
        var token = jwt.sign({ id }, 'jwt-secret-x9', { expiresIn: 300 });//5min
        res.status(200).send({ auth: true, token: token })
    }
    res.status(401).send('login invalido')
});

app.listen(3001, () => console.log(`API 01 listening on port 3001!`));