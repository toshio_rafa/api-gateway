const jwt = require('jsonwebtoken');


function authorize(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (!token) {
        res.status(401).json({
            message: 'Acesso Restrito'
        });
    } else {
        jwt.verify(token, 'jwt-secret-x9', function (error, decoded) {
            if (error) {
                res.status(401).json({
                    message: 'Token Inválido'
                });
            } else {
                req.headers['user_id'] = '6464564-654654-65654654'
                next();
            }
        });
    }
};

module.exports = { authorize }