const express = require('express');
const url = require('url')
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
const bodyParser = require('body-parser');
const httpProxy = require('express-http-proxy');
const { createProxyMiddleware } = require('http-proxy-middleware')
const app = express();
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const helmet = require('helmet')
const port = 3000;

const {
  USERS_API_URL,
  PRODUCTS_API_URL,
} = require('./URLs');

const authService = require('./AuthService');
const userServiceProxy = httpProxy(USERS_API_URL, {
  proxyReqPathResolver: req => url.parse(req.url).path
})
const productsServiceProxy = httpProxy(PRODUCTS_API_URL);

app.use(bodyParser.json({
  limit: '5mb'
}));
app.use(bodyParser.urlencoded({
  extended: false
}));

// GraphQL schema
var schema = buildSchema(` 
    type Query {
        message: String
    }
`);
// Root resolver
var root = {
  message: () => 'Hello World pra nois!'
};
// Create an express server and a GraphQL endpoint
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

app.use(logger('dev'))
app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.get('/', (req, res) => res.send('Hello Gateway API'));

const apiProxy = createProxyMiddleware('/users', { target: 'http://localhost:3001', changeOrigin: true });

app.use(apiProxy)
// app.use('/users/**', (req, res, next) => {
//   console.log('originalUrl', req.originalUrl)
//   userServiceProxy(req, res, next)
// })

//api 1
app.post('/login', (req, res, next) => userServiceProxy(req, res, next));
// app.get('/users', authService.authorize, (req, res, next) => userServiceProxy(req, res, next));


//api 2
app.use('/products', authService.authorize, (req, res, next) => productsServiceProxy(req, res, next));



app.listen(port, () => console.log(`Example app listening on port ${port}!`));

